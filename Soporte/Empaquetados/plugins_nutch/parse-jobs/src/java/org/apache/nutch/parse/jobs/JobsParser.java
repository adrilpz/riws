/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.nutch.parse.jobs;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.avro.util.Utf8;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.parse.HTMLMetaTags;
import org.apache.nutch.parse.Parse;
import org.apache.nutch.parse.ParseFilter;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.nutch.util.Bytes;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.w3c.dom.DocumentFragment;

/**
 * Parse HTML meta tags (keywords, description) and store them in the parse
 * metadata so that they can be indexed with the index-metadata plugin with the
 * prefix 'metatag.'. Metatags are matched ignoring case.
 */
public class JobsParser implements ParseFilter {

  private static final Log LOG = LogFactory.getLog(JobsParser.class
      .getName());

  private Configuration conf;

  public static final String PARSE_META_PREFIX = "job_";

  private Set<String> metatagset = new HashSet<String>();

  public void setConf(Configuration conf) {
    this.conf = conf;
    // specify whether we want a specific subset of metadata
    // by default take everything we can find
    String[] values = conf.getStrings("jobtags.names", "*");
    for (String val : values) {
      metatagset.add(val.toLowerCase(Locale.ROOT));
    }
  }

  public Configuration getConf() {
    return this.conf;
  }

  /**
   * Check whether the metatag is in the list of metatags to be indexed (or if
   * '*' is specified). If yes, add it to parse metadata.
   */
  private void addIndexedMetatags(Map<CharSequence, ByteBuffer> metadata,
      String metatag, String value) {
    String lcMetatag = metatag.toLowerCase(Locale.ROOT);
    if (metatagset.contains("*") || metatagset.contains(lcMetatag)) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Found meta tag: " + lcMetatag + "\t" + value);
      }
      metadata.put(new Utf8(PARSE_META_PREFIX + lcMetatag),
          ByteBuffer.wrap(value.getBytes()));
    }
  }

  public Parse filter(String url, WebPage page, Parse parse,
      HTMLMetaTags metaTags, DocumentFragment doc) {

	  String contenido = Bytes.toString(page.getContent().array());
	  Document document = Jsoup.parse(contenido);
	  
	  String location = null;
	  String title = null;
	  String description = null;
	  String duration = null;
    String date = null;
	  
	  if (document.getElementById("parLocation") != null)
		  location = document.getElementById("parLocation").text();
	  if(document.getElementById("hdgJobTitle") !=null) 
		 title = document.getElementById("hdgJobTitle").text();
	  if(document.getElementById("parDescription") != null)
		 description = document.getElementById("parDescription").text();
	  if(document.getElementById("parDuration") != null)
		 duration = document.getElementById("parDuration").text();
    if(document.getElementById("parDatePosted") != null)
     date = document.getElementById("parDatePosted").text();
			  
	  
	  if(location != null && !location.isEmpty())
		  addIndexedMetatags(page.getMetadata(), "location", location);
	  
	  if(title != null && !title.isEmpty())
		  addIndexedMetatags(page.getMetadata(), "name", title); 
	  
	  if(description != null && !description.isEmpty())
		  addIndexedMetatags(page.getMetadata(), "description", description); 
	  
	  if(url != null && !url.isEmpty())
		  addIndexedMetatags(page.getMetadata(), "url", url); 
	  
	  if(duration != null && !duration.isEmpty())
		  addIndexedMetatags(page.getMetadata(), "duration", duration);

    if(date != null && !date.isEmpty())
      addIndexedMetatags(page.getMetadata(), "date", date); 

    return parse;
  }

  @Override
  public Collection<Field> getFields() {
    return null;
  }

}