package es.udc.master.riws.search;

import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import es.udc.master.riws.job.Job;

@Controller
public class SearchController {

	/** The page size. */
	@Value("${page.size}")
	private int pageSize;

	@Autowired
	private SearchService searchService;

	/** The Constant ERROR_PAGE. */
	private static final String ERROR_PAGE = "error/general";

	/** The logger. */
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@GetMapping("/")
	public String index(Model model) {
		try {
			model.addAttribute("locations", searchService.getDifferentLocs());
		} catch (SolrServerException e) {
			String errorMessage = "It is not possible to load locations";
			return logAndDisplayException(errorMessage, model, e);
		}
		return "search/results";
	}

	@GetMapping("search/results")
	public String getResults(Model model, @RequestParam(required = true, name = "keywords") String keywords,
			@RequestParam(required = true, name = "selectedLoc") String selectedLoc, Pageable pageable) {
		try {
			Page<Job> resultsPage;
			PageWrapper<Job> page;
			model.addAttribute("keywords", keywords);
			model.addAttribute("selectedLoc", selectedLoc);
			if (keywords != null && !keywords.equals("")) {
				if (selectedLoc != null && !selectedLoc.equals("") && !selectedLoc.equals("All")) {
					resultsPage = searchService.findByKeywordsAndLoc(pageable, keywords, selectedLoc);
					page = new PageWrapper<>(resultsPage,
							"/search/results?keywords=" + keywords + "&selectedLoc=" + selectedLoc, pageSize);
				} else {
					resultsPage = searchService.findByKeywords(pageable, keywords);
					page = new PageWrapper<>(resultsPage,
							"/search/results?keywords=" + keywords + "&selectedLoc=" + selectedLoc, pageSize);
				}
			} else {
				if (selectedLoc != null && !selectedLoc.equals("") && !selectedLoc.equals("All")) {
					resultsPage = searchService.findByLoc(pageable, selectedLoc);
					page = new PageWrapper<>(resultsPage,
							"/search/results?keywords=" + keywords + "&selectedLoc=" + selectedLoc, pageSize);
				} else {
					resultsPage = searchService.findAll(pageable);
					page = new PageWrapper<>(resultsPage,
							"/search/results?keywords=" + keywords + "&selectedLoc=" + selectedLoc, pageSize);
				}
			}
			model.addAttribute("results", page.getContent());
			model.addAttribute("page", page);
			model.addAttribute("locations", searchService.getDifferentLocs());
		} catch (SolrServerException e) {
			String errorMessage = "It is not possible to get results";
			return logAndDisplayException(errorMessage, model, e);
		}

		return "search/results";
	}

	private String logAndDisplayException(String errorMessage, Model model, Throwable e) {
		logger.warn(errorMessage, e);
		model.addAttribute("errorMessage", errorMessage);
		return ERROR_PAGE;
	}
}
