package es.udc.master.riws.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import es.udc.master.riws.job.Job;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SearchService {

	private final static String URL_CONEXION_SOLR = "http://localhost:8983/solr/jobs";

	private HttpSolrServer server = null;

	private HttpSolrServer getSolrServer() {
		if (server == null) {
			server = new HttpSolrServer(URL_CONEXION_SOLR);
			server.setMaxRetries(1); // defaults to 0. > 1 not recommended.
			server.setConnectionTimeout(5000); // 5 seconds to establish TCP

			// Setting the XML response parser is only required for cross
			// version compatibility and only when one side is 1.4.1 or
			// earlier and the other side is 3.1 or later.
			server.setParser(new XMLResponseParser()); // binary parser is used
														// by
														// default
			// The following settings are provided here for completeness.
			// They will not normally be required, and should only be used
			// after consulting javadocs to know whether they are truly
			// required.
			server.setSoTimeout(1000); // socket read timeout
			server.setDefaultMaxConnectionsPerHost(100);
			server.setMaxTotalConnections(100);
			server.setFollowRedirects(false); // defaults to false
			// allowCompression defaults to false.
			// Server side must support gzip or deflate for this to have any
			// effect.
			server.setAllowCompression(true);
		}

		return server;
	}

	public Page<Job> findByKeywords(Pageable pageable, String keywords) throws SolrServerException {
		SolrQuery query = new SolrQuery();
		
		String[] keywordsList = keywords.split("\\s");
		
		String queryKeywords = "";
		for (String keyword: keywordsList) {
			if (keyword.equals(keywordsList[0])) {
				queryKeywords = queryKeywords + "*" + keyword + "* ";
			} else {
				queryKeywords = queryKeywords + "&& *" + keyword + "* ";
			}
		}
		
		String queryString = "(job_description:" + queryKeywords + ") || (job_name:" + queryKeywords +")";
		query.setQuery(queryString);

		// Configuring page of results
		int start = pageable.getPageNumber() * pageable.getPageSize();
		query.setStart(start);
		query.setRows(pageable.getPageSize());

		// Query the server
		QueryResponse rsp = getSolrServer().query(query);

		// Read documents as beans.
		List<Job> beans = rsp.getBeans(Job.class);

		// Get total number of results
		long numResults = rsp.getResults().getNumFound();

		return new PageImpl<>(beans, pageable, numResults);
	}

	public Page<Job> findByKeywordsAndLoc(Pageable pageable, String keywords, String location)
			throws SolrServerException {
SolrQuery query = new SolrQuery();
		
		String[] keywordsList = keywords.split("\\s");
		
		String queryKeywords = "";
		for (String keyword: keywordsList) {
			if (keyword.equals(keywordsList[0])) {
				queryKeywords = queryKeywords + "*" + keyword + "* ";
			} else {
				queryKeywords = queryKeywords + "&& *" + keyword + "* ";
			}
		}
		
		String queryString = "((job_description:" + queryKeywords + ") || (job_name:" + queryKeywords +")) && job_location:\"" + location + "\"";
		query.setQuery(queryString);

		// Configuring page of results
		int start = pageable.getPageNumber() * pageable.getPageSize();
		query.setStart(start);
		query.setRows(pageable.getPageSize());

		// Query the server
		QueryResponse rsp = getSolrServer().query(query);

		// Read documents as beans.
		List<Job> beans = rsp.getBeans(Job.class);

		// Get total number of results
		long numResults = rsp.getResults().getNumFound();

		return new PageImpl<>(beans, pageable, numResults);
	}

	public Page<Job> findByLoc(Pageable pageable, String location) throws SolrServerException {
		SolrQuery query = new SolrQuery();
		query.setQuery("job_location:\"" + location + "\"");

		// Configuring page of results
		int start = pageable.getPageNumber() * pageable.getPageSize();
		query.setStart(start);
		query.setRows(pageable.getPageSize());

		// Query the server
		QueryResponse rsp = getSolrServer().query(query);

		// Read documents as beans.
		List<Job> beans = rsp.getBeans(Job.class);

		// Get total number of results
		long numResults = rsp.getResults().getNumFound();

		return new PageImpl<>(beans, pageable, numResults);
	}

	public Page<Job> findAll(Pageable pageable) throws SolrServerException {
		SolrQuery query = new SolrQuery();
		query.setQuery("job_name:*");

		// Configuring page of results
		int start = pageable.getPageNumber() * pageable.getPageSize();
		query.setStart(start);
		query.setRows(pageable.getPageSize());

		// Query the server
		QueryResponse rsp = getSolrServer().query(query);

		// Read documents as beans.
		List<Job> beans = rsp.getBeans(Job.class);

		// Get total number of results
		long numResults = rsp.getResults().getNumFound();

		return new PageImpl<>(beans, pageable, numResults);
	}

	public List<String> getDifferentLocs() throws SolrServerException {
		SolrQuery query = new SolrQuery();

		// Get total number of results
		query.setQuery("*:*");
		query.setStart(0);
		QueryResponse rsp = getSolrServer().query(query);
		Long numResults = rsp.getResults().getNumFound();

		// Query
		query.setQuery("*:*");
		query.setStart(0);
		query.setRows(numResults.intValue());

		// Query the server
		rsp = getSolrServer().query(query);

		// Read documents as beans.
		List<Job> beans = rsp.getBeans(Job.class);

		List<String> locations = new ArrayList<String>();
		for (Job j : beans) {
			if (j.getLocation() != null & !locations.contains(j.getLocation())) {
				locations.add(j.getLocation());
			}
		}
		Collections.sort(locations);
		locations.add(0,"All");

		return locations;
	}

}
