package es.udc.master.riws.job;

import org.apache.solr.client.solrj.beans.Field;

public class Job {

	// hdgJobTitle
	@Field("job_name")
	private String name;

	// parDescription
	@Field("job_description")
	private String description;

	// parLocation
	@Field("job_location")
	private String location;

	// Url
	@Field("job_url")
	private String url;

	// parDuration
	@Field("job_duration")
	private String duration;

	// parDatePosted
	@Field("job_date")
	private String date;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
}
